#!/bin/bash

set -e -x

RADAR_HOME=$(dirname $0)
PROJECT=adsb-radar

cd ${RADAR_HOME}

docker build -t adsb-radar .

cd modules

docker-compose -p $PROJECT down
docker-compose -p $PROJECT --profile notebook --profile cache up -d --build --scale validator=6 $*

sleep 5

docker-ascii-map

sleep 30
docker-compose -p $PROJECT logs notebook 2>&1 | grep token
docker-compose -p $PROJECT exec postgres psql -U adsb

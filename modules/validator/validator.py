import logging
import sys

from adsb import AdsbFilter, AdsbAccuracyLogger
from messaging import RedisTopicPublisher, RedisQueueConsumer

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    adsb_monitor = AdsbAccuracyLogger()
    adsb_monitor.start()

    adsb_producer = RedisTopicPublisher(host=sys.argv[1], channel=sys.argv[3])
    adsb_filter = AdsbFilter(processor=adsb_producer, monitor=adsb_monitor, max_correction=1)
    adsb_consumer = RedisQueueConsumer(host=sys.argv[1], listname=sys.argv[2], processor=adsb_filter)
    adsb_consumer.start()

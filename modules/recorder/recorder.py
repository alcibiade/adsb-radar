import datetime
import logging
import sys

import psycopg2

from adsb import AdsbProcessor
from messaging import RedisTopicConsumer
from model import AdsbMessage


class AdsbRecorder(AdsbProcessor):

    def __init__(self, pghost, pgdb, pguser, pgpass):
        super(AdsbRecorder, self).__init__()
        self.conn = psycopg2.connect(host=pghost, database=pgdb, user=pguser, password=pgpass)
        cur = self.conn.cursor()
        cur.execute('''
        create table if not exists message (
            id bigserial primary key,
            message varchar(255) not null,
            ts timestamp not null,
            identifier varchar(255) not null,
            lat float not null,
            lon float not null
        )
        ''')
        self.conn.commit()
        cur.close()

    def process_adsb(self, message: AdsbMessage):
        with self.conn.cursor() as cur:
            cur.execute('insert into message (ts, message, identifier, lat, lon) values (%s, %s, %s, %s, %s)',
                        (datetime.datetime.fromtimestamp(message.timestamp), message.message, message.identifier,
                         message.location[0], message.location[1]))

            self.conn.commit()

        # print(msg)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    adsb_recorder = AdsbRecorder(sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])

    adsb_consumer = RedisTopicConsumer(host=sys.argv[1], channel=sys.argv[2], processor=adsb_recorder)
    adsb_consumer.start()

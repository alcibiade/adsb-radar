import logging
import sys
import time
import uuid
from threading import Thread

import psycopg2

from adsb import AdsbParser
from model import TrackingContext, AdsbMessage


class DataConsolidation(Thread):

    def __init__(self, pghost: str, pgdb: str, pguser: str, pgpass: str, limit: int = 1000):
        super(DataConsolidation, self).__init__()
        self.conn = psycopg2.connect(host=pghost, database=pgdb, user=pguser, password=pgpass)
        self.log = logging.getLogger('DataConsolidation')
        cur = self.conn.cursor()
        cur.execute('''
        create table if not exists message_data (
            id bigint primary key,
            message varchar(255) not null,
            ts timestamp not null,
            track char(32) not null,
            tc int,
            category varchar(3),
            parity int,
            icao varchar(16),
            callsign varchar(16),
            lat float,
            lon float,
            altitude float,
            heading float,
            speed float,
            vspeed float,
            sptype varchar(6),
            version int 
        )
        ''')
        self.limit = limit
        self.conn.commit()
        cur.close()

    def run(self) -> None:

        while True:
            insertions = 0

            with self.conn.cursor() as cur_in, self.conn.cursor() as cur_out, self.conn.cursor() as cur_track:
                cur_in.execute('select max(id) from message_data')
                latest_id = cur_in.fetchone()[0] or 0
                self.log.debug('Latest processed message id is %8d', latest_id)
                cur_in.execute('select id, message, ts from message where message.id > %s order by id limit %s',
                               (latest_id, self.limit))

                while True:
                    record = cur_in.fetchone()

                    if not record:
                        break

                    context = TrackingContext()
                    parser = AdsbParser(context)
                    data = parser.process_adsb(AdsbMessage(record[2].timestamp(), record[1]))

                    cur_track.execute('select ts, track from message_data where icao=%s order by ts desc limit 1',
                                      (data.icao,))

                    previous_message = cur_track.fetchone()

                    if previous_message and abs(previous_message[0].timestamp() - record[2].timestamp()) < 3600:
                        track = previous_message[1]
                    else:
                        track = uuid.uuid4().hex

                    cur_out.execute('insert into message_data(id, message, ts, track, tc, icao, callsign,'
                                    ' lat, lon, altitude, heading, speed, vspeed,'
                                    ' sptype, version, category, parity)'
                                    ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                                    (record[0], record[1], record[2], track, data.tc, data.icao, data.callsign,
                                     data.lat, data.lon, data.altitude, data.heading, data.speed, data.vspeed,
                                     data.sptype, data.version, data.category, data.parity))
                    insertions += 1

                self.conn.commit()

            if insertions < self.limit:
                time.sleep(5)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    consolidation = DataConsolidation(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    consolidation.start()

from unittest import TestCase

from model import AdsbMessage, from_json


class TestModel(TestCase):

    def test_message_equality(self):
        self.assertEqual(AdsbMessage(0, 'xxx'), AdsbMessage(0, 'xxx'))
        self.assertNotEqual(AdsbMessage(0, 'xxx'), AdsbMessage(1, 'xxx'))
        self.assertNotEqual(AdsbMessage(0, 'xxx'), AdsbMessage(0, 'yyy'))

    def test_message_serialization(self):
        data = 'Hello world !'
        message1 = AdsbMessage(timestamp=0, message=data)
        json = message1.to_json()
        # print(json)
        message2 = from_json(json)
        self.assertEqual(message1.message, message2.message)

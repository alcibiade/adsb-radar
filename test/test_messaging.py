import os
import time
from typing import Optional
from unittest import TestCase

from adsb import AdsbProcessor
from messaging import RedisQueuePublisher, RedisQueueConsumer, RedisTopicPublisher, RedisTopicConsumer
from model import AdsbMessage, AdsbMessageData

REDIS_HOST = os.environ.get('REDIS_HOST')


class AdsbStorage(AdsbProcessor):
    def __init__(self):
        self.message = None

    def process_adsb(self, message: AdsbMessage) -> Optional[AdsbMessageData]:
        self.message = message
        return None


class TestMessaging(TestCase):

    def test_transfer_queue(self):
        storage = AdsbStorage()
        adsb_publisher = RedisQueuePublisher(host=REDIS_HOST, listname='test1')
        adsb_receiver = RedisQueueConsumer(host=REDIS_HOST, listname='test1', processor=storage)

        adsb_receiver.daemon = True
        adsb_receiver.start()

        message1 = AdsbMessage(timestamp=time.time(), message='Hello world !', location=(2.1, 4.3), identifier='ACME')

        time.sleep(1)
        adsb_publisher.process_adsb(message=message1)
        time.sleep(1)

        self.assertEqual(message1, storage.message)

    def test_transfer_topic(self):
        storage = AdsbStorage()
        adsb_publisher = RedisTopicPublisher(host=REDIS_HOST, channel='test2')
        adsb_receiver = RedisTopicConsumer(host=REDIS_HOST, channel='test2', processor=storage)

        adsb_receiver.daemon = True
        adsb_receiver.start()

        message1 = AdsbMessage(timestamp=time.time(), message='Hello world !', location=(2.1, 4.3), identifier='ACME')

        time.sleep(1)
        adsb_publisher.process_adsb(message=message1)
        time.sleep(1)

        self.assertEqual(message1, storage.message)

from unittest import TestCase

from correction import correct
from model import AdsbMessage


def correct_str(msg: str, max_correction: int):
    message = AdsbMessage(timestamp=0, message=msg)
    result = correct(message, max_correction=max_correction)
    return result[0].message if result[0] is not None else None, result[1]


# noinspection PyUnresolvedReferences
class TestCorrection(TestCase):

    def test_correct_message(self):
        self.assertEqual(('8d45ce555859b52c0d5678ea6cf1', 0),
                         correct_str('8d45ce555859b52c0d5678ea6cf1', max_correction=0))
        self.assertEqual(('8d45ce555859b52c0d5678ea6cf1', 0),
                         correct_str('8d45ce555859b52c0d5678ea6cf1', max_correction=1))
        self.assertEqual(('8d45ce555859b52c0d5678ea6cf1', 0),
                         correct_str('8d45ce555859b52c0d5678ea6cf1', max_correction=2))
        self.assertEqual(('8d45ce555859b52c0d5678ea6cf1', 0),
                         correct_str('8d45ce555859b52c0d5678ea6cf1', max_correction=3))

    def test_broken_message(self):
        self.assertEqual((None, 99), correct_str('80121717c6500030a4000088161c', max_correction=0))
        self.assertEqual((None, 99), correct_str('80121717c6500030a4000088161c', max_correction=1))
        self.assertEqual((None, 99), correct_str('80121717c6500030a4000088161c', max_correction=2))
        self.assertEqual((None, 99), correct_str('80121717c6500030a4000088161c', max_correction=3))

    def test_single_error(self):
        self.assertEqual((None, 99), correct_str('8da37b459909d58ed8381fef705b', max_correction=0))
        self.assertEqual(('8da37b459909d58ed8781fef705b', 1),
                         correct_str('8da37b459909d58ed8381fef705b', max_correction=1))
        self.assertEqual(('8da37b459909d58ed8781fef705b', 1),
                         correct_str('8da37b459909d58ed8381fef705b', max_correction=2))
        self.assertEqual(('8da37b459909d58ed8781fef705b', 1),
                         correct_str('8da37b459909d58ed8381fef705b', max_correction=3))

    def test_double_error(self):
        self.assertEqual((None, 99), correct_str('8d4993d49911b7941824272aa329', max_correction=0))
        self.assertEqual((None, 99), correct_str('8d4993d49911b7941824272aa329', max_correction=1))
        self.assertEqual(('8d49d3d49911b7941804272aa329', 2),
                         correct_str('8d4993d49911b7941824272aa329', max_correction=2))
        self.assertEqual(('8d49d3d49911b7941804272aa329', 2),
                         correct_str('8d4993d49911b7941824272aa329', max_correction=3))

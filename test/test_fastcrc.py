from unittest import TestCase

import pyModeS as pms

from fastcrc import BytesWrapper, crc, GENERATOR


# noinspection PyUnresolvedReferences
class TestFastCrc(TestCase):

    def test_wrapper(self):
        self.assertEqual(1, BytesWrapper('01010101').byte_count())
        self.assertEqual(0, BytesWrapper('01FF01FF').get_bit(0, 0))
        self.assertEqual(0, BytesWrapper('01FF01FF').get_bit(0, 3))
        self.assertEqual(1, BytesWrapper('01FF01FF').get_bit(0, 7))
        self.assertEqual(1, BytesWrapper('01FF01FF').get_bit(1, 0))
        self.assertEqual(1, BytesWrapper('01FF01FF').get_bit(3, 7))

        wrapper = BytesWrapper('00000000')
        wrapper.apply_matrix(0, 0)
        self.assertEqual(GENERATOR, wrapper._bytes)

        wrapper = BytesWrapper('0000000000')
        wrapper.apply_matrix(1, 0)
        self.assertEqual([0] + GENERATOR, wrapper._bytes)

    def test_cases(self):
        self.assertEqual(0, crc('8d8960ed58bf053cf11bc5932b7d'))
        self.assertEqual(0, crc('8d45cab390c39509496ca9a32912'))
        self.assertEqual(0, crc('8d49d3d4e1089d00000000744c3b'))
        self.assertEqual(0, crc('8d74802958c904e6ef4ba0184d5c'))
        self.assertEqual(0, crc('8d4400cd9b0000b4f87000e71a10'))
        self.assertEqual(0, crc('8d4065de58a1054a7ef0218e226a'))

        self.assertEqual(10719924, crc('c80b2dca34aa21dd821a04cb64d4'))
        self.assertEqual(4805588, crc('a800089d8094e33a6004e4b8a522'))
        self.assertEqual(5659991, crc('a8000614a50b6d32bed000bbe0ed'))
        self.assertEqual(11727682, crc('a0000410bc900010a40000f5f477'))
        self.assertEqual(16, crc('8d4ca251204994b1c36e60a5343d'))
        self.assertEqual(353333, crc('b0001718c65632b0a82040715b65'))

    def test_samples(self):
        with open('samples/adsb-samples-01.txt', 'r') as f:
            lines = f.readlines()
            for msg in lines:
                adsb = msg.strip('*;\r\n')
                self.assertEqual(pms.crc(adsb), crc(adsb))

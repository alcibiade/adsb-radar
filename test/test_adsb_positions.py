import datetime
from unittest import TestCase

from adsb import AdsbParser
from model import TrackingContext, AdsbMessage
from record import AdsbPlayer


class TestAdsbPositions(TestCase):

    def test_log_replay(self):
        tracking_context = TrackingContext()
        adsb_parser = AdsbParser(tracking_context)
        producer = AdsbPlayer(adsb_parser, 'samples/adsb-locationjumps.log')
        producer.run()

        for ac in tracking_context.aircrafts:
            positions = ac.trajectory.positions
            if len(positions) > 1:

                for pos_index in range(1, len(positions)):
                    dist = positions[pos_index].distance(positions[pos_index - 1])
                    # a step over 1 degree is a symptom of coordinates parsing error
                    self.assertLess(dist, 1, 'Inconsistent trajectory for flight ' + str(ac) + ' from '
                                    + str(positions[pos_index - 1]) + ' to ' + str(positions[pos_index]))

    def test_location_bug(self):
        """These flight recordings yielded bogus flight positions in the code."""
        tracking_context = TrackingContext()
        adsb_parser = AdsbParser(tracking_context)

        for test_file_name in ['samples/location-bug-01.csv', 'samples/location-bug-02.csv']:
            with open(test_file_name) as f:
                for line in f.readlines():
                    fields = line.split('\t')

                    id = fields[0]
                    msg = fields[1]
                    dt_text = fields[2]
                    # print(id, dt_text, msg)

                    # ISO format does not support unpadded values
                    while len(dt_text) < 26:
                        dt_text = dt_text + '0'

                    dt = datetime.datetime.fromisoformat(dt_text)

                    message_data = adsb_parser.process_adsb(AdsbMessage(timestamp=0, message=msg))
                    # print(message_data.lon)

                    # Longitude was never more than 8 while we got a report of 136.3 for message 407938
                    if message_data.lon and message_data.lon > 10:
                        print(id, msg, dt, message_data)

                    # if message_data.lon is not None:
                    #     self.assertLess(4, message_data.lon)
                    #     self.assertLess(message_data.lon, 8)

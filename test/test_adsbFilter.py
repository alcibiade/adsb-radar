from unittest import TestCase
from unittest.mock import patch

from adsb import AdsbFilter, AdsbAccuracyLogger
from model import AdsbMessage


# noinspection PyUnresolvedReferences
class TestAdsbFilter(TestCase):

    def test_filtering_level0(self):
        self._test_filter(0, 29, ' 94.2%')

    def test_filtering_level1(self):
        self._test_filter(1, 42, ' 91.6%')

    # def test_filtering_level2(self):
    #     self._test_filter(2, 52, ' 89.6%')

    def _test_filter(self, correction, records, error_rate):
        with patch('adsb.AdsbProcessor') as pro:
            monitor = AdsbAccuracyLogger()
            adsb_filter = AdsbFilter(pro, monitor, max_correction=correction)
            with open('samples/adsb-samples-01.txt', 'r') as f:
                lines = f.readlines()
                for msg in lines:
                    adsb_filter.process_adsb(AdsbMessage(timestamp=0, message=msg.strip('*;\r\n')))
            self.assertEqual(records, pro.process_adsb.call_count)
            self.assertEqual(error_rate, monitor.get_error_rate_as_string())

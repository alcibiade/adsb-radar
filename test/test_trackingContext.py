from unittest import TestCase
from unittest.mock import patch

from model import TrackingContext, Aircraft, AircraftTrajectory, AircraftPosition


class TestTrackingContext(TestCase):

    @patch('model.TrackingObserver')
    def test_notification(self, observerMock):
        context = TrackingContext()
        context.addObserver(observerMock)

        ac = Aircraft(ts=8000, icao='123456', callsign='AA3333')
        context.notify_update(0, ac)

        observerMock.aircraft_updated.assert_called_once_with(ac)
        observerMock.aircraft_lost.assert_not_called()

        context.notify_lost(ac)
        observerMock.aircraft_updated.assert_called_once_with(ac)
        observerMock.aircraft_lost.assert_called_once_with(ac)

    @patch('model.TrackingObserver')
    def test_lost(self, observerMock):
        context = TrackingContext()
        context.addObserver(observerMock)

        ac = context.getAircraft(ts=8000, icao='123456')
        context.detect_lost_aircrafts(ts=8000)
        self.assertEqual(1, len(context.aircrafts))
        observerMock.aircraft_updated.assert_not_called()
        observerMock.aircraft_lost.assert_not_called()

        ac.lastmessage = 0
        context.detect_lost_aircrafts(ts=8000)
        observerMock.aircraft_updated.assert_not_called()
        observerMock.aircraft_lost.assert_called_once_with(ac)

    def test_trajectory(self):
        trajectory = AircraftTrajectory()
        self.assertEqual(trajectory.positions, [])

        trajectory.register_position(0, 2, 2, None)
        self.assertEqual(trajectory.positions, [])

        trajectory.register_position(0, 2, 2, 4)
        self.assertEqual(len(trajectory.positions), 1)

        trajectory.register_position(0, 2, 2, 4)
        self.assertEqual(len(trajectory.positions), 1)

        trajectory.register_position(0, 2, 5, 4)
        self.assertEqual(len(trajectory.positions), 2)

        trajectory.register_position(0, 2, 5, 7)
        self.assertEqual(len(trajectory.positions), 2)
        self.assertEqual(trajectory.positions[0].alt, 4)
        self.assertEqual(trajectory.positions[1].alt, 7)

    def test_distance(self):
        self.assertEqual(AircraftPosition(0, 1, 2, 1).distance(AircraftPosition(1, 1, 2, 1)), 0)
        self.assertEqual(AircraftPosition(0, 1, 2, 1).distance(AircraftPosition(1, 2, 2, 1)), 1)
        self.assertEqual(AircraftPosition(0, 1, 2, 1).distance(AircraftPosition(1, 1, 1, 1)), 1)
        self.assertEqual(AircraftPosition(0, 1, 2, 1).distance(AircraftPosition(1, 1, 5, 1)), 3)
        self.assertAlmostEqual(AircraftPosition(0, 1, 2, 1).distance(AircraftPosition(1, 3, 1, 1)), 2.236, places=3)

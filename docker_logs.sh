#!/bin/bash

set -e -x

RADAR_HOME=$(dirname $0)
PROJECT=adsb-radar

cd ${RADAR_HOME}/modules
docker-compose -p $PROJECT logs -f

from threading import Lock
from typing import Optional, Tuple, Callable

lock = Lock()


class NonBlockingQueue:

    def __init__(self, capacity: int, on_enter: Callable = None, on_leave: Callable = None):
        self.items = [None] * capacity
        self.head = 0
        self.capacity = capacity
        self.full = False
        self.on_enter = on_enter
        self.on_leave = on_leave

    def put(self, item: any):
        with lock:
            current_item = self.items[self.head]

            if self.on_leave is not None and current_item is not None:
                self.on_leave(current_item)

            if self.on_enter is not None:
                self.on_enter(item)

            self.items[self.head] = item
            self.head = self.head + 1

            if self.head == self.capacity:
                self.full = True
                self.head = 0

    def peek(self) -> Tuple[Optional[any], int]:
        with lock:
            return (self.items[self.head], self.capacity) if self.full else (self.items[0], self.head)

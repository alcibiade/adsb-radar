from argparse import ArgumentParser

from adsb import *
from messaging import RedisTopicConsumer
from model import TrackingContext
from record import AdsbRecorder, AdsbPlayer

if __name__ == '__main__':
    logging.basicConfig(level=logging.WARN,
                        format='%(asctime)s:%(threadName)-12s:%(levelname)s:%(name)-12s:%(message)s')
    tracking_context = TrackingContext()

    parser = ArgumentParser(description='An ADSB radar display.')
    parser.add_argument('-r', '--redis-host', help='Read messages from a remote redis host')
    parser.add_argument('-c', '--redis-channel', help='Channel name on the remote redis instance', default='adsb')
    parser.add_argument('-i', '--input', help='Input file from previously recorded session')
    parser.add_argument('-o', '--output', help='Output messages and timestamps to a record file')
    parser.add_argument('engine', help='Render engine to use: cairo or curses', default='curses')

    namespace = parser.parse_args()

    adsb_monitor = AdsbAccuracyLogger()
    adsb_monitor.start()

    adsb_parser = AdsbParser(tracking_context)
    adsb_recorder = AdsbRecorder(adsb_parser, record_file=namespace.output)
    adsb_filter = AdsbFilter(adsb_recorder, adsb_monitor, max_correction=0)

    if namespace.redis_host is not None:
        adsb_receiver = RedisTopicConsumer(host=namespace.redis_host,
                                           processor=adsb_filter,
                                           channel=namespace.redis_channel)
        thread = Thread(target=adsb_receiver.run,
                        daemon=True,
                        name='RedisTopicConsumer')
        thread.start()
    elif namespace.input is not None:
        adsb_receiver = AdsbPlayer(processor=adsb_filter,
                                   record_file=namespace.input)
        thread = Thread(target=adsb_receiver.run,
                        daemon=True,
                        name='AdsbPlayer')
        thread.start()
    else:
        adsb_receiver = AdsbReceiver(adsb_filter, identifier='radar-gui', lat=0, lon=0)
        adsb_receiver.start()

    # Run the local UI

    if namespace.engine == 'cairo':
        from ui.cairo_ui import CairoUI

        ui = CairoUI(tracking_context, adsb_monitor)
        ui.run()
    else:
        from ui.curses_ui import CursesUI

        center = poi.center_coord
        ui = CursesUI(tracking_context, adsb_monitor, center)
        ui.run()

    adsb_receiver.interrupt()

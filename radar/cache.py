import json
import logging
import time

from redis import StrictRedis

from model import TrackingObserver, Aircraft


class CacheManager(TrackingObserver):
    def __init__(self, host: str, key: str):
        super(TrackingObserver).__init__()
        self.log = logging.getLogger('CacheManager')
        self.r = StrictRedis(host=host)
        self.key = key
        self.log.debug('Cache manager initialized: host=%s key=%s' % (host, key))

    def aircraft_lost(self, aircraft: Aircraft):
        self.log.debug('  Lost: ' + str(aircraft))
        self.r.set('plane-' + aircraft.icao, json.dumps(aircraft.getJsonData(time.time())))

    def aircraft_updated(self, aircraft: Aircraft):
        self.log.debug('Update: ' + str(aircraft))
        if aircraft.lat and aircraft.lon:
            self.log.debug('%s %f %f %s' % (self.key, aircraft.lon, aircraft.lat, aircraft.icao))
            self.r.geoadd(self.key, aircraft.lon, aircraft.lat, aircraft.icao)

        self.r.set('plane-' + aircraft.icao, json.dumps(aircraft.getJsonData(time.time())))

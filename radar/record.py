from adsb import AdsbProcessor, AdsbProducer
from model import AdsbMessage


class AdsbRecorder(AdsbProcessor, AdsbProducer):

    def __init__(self, processor: AdsbProcessor, record_file: str = None):
        if record_file is None:
            record_file = '/dev/null'

        AdsbProducer.__init__(self, processor)
        self.f = open(record_file, 'w')

    def process_adsb(self, message: AdsbMessage):
        self.f.write('%f %s\n' % (message.timestamp, message.message))
        self.next_processor.process_adsb(message)


class AdsbPlayer(AdsbProducer):

    def __init__(self, processor: AdsbProcessor, record_file: str):
        AdsbProducer.__init__(self, processor)
        self.record_file = record_file
        self._interrupted = False

    def run(self):
        with open(self.record_file) as f:
            for line in f.readlines():
                if self._interrupted:
                    break

                ts, msg = line.split()
                ts = float(ts)

                adsb_message = AdsbMessage(timestamp=ts, message=msg)

                self.next_processor.process_adsb(adsb_message)

    def interrupt(self):
        self._interrupted = True

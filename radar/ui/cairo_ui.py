import logging
import math
import time
from threading import Thread

import cairo
import gi
from cairo._cairo import Context

from adsb import AdsbAccuracyMonitor

gi.require_version("Gtk", "3.0")
from gi.overrides.Gdk import Gdk
from gi.repository import Gtk
from gi.repository import GLib

from ui.map import Projection
from model import TrackingContext, TrackingObserver, Aircraft
from poi import center_coord, radius, cities, airport_coords, waypoints, routes


class LostAircraftCollector(Thread):

    def __init__(self, context: TrackingContext):
        Thread.__init__(self, name='LostAircraftCollector', daemon=True)
        self.context = context

    def run(self):
        while True:
            ts = time.time()
            self.context.detect_lost_aircrafts(ts)
            time.sleep(10)


class CairoUI(TrackingObserver):

    def __init__(self, tracking_context: TrackingContext, adsb_monitor: AdsbAccuracyMonitor):

        self.tracking_context = tracking_context
        self.tracking_context.addObserver(self)

        self.adsb_monitor = adsb_monitor

        self.log = logging.getLogger('Cairo UI')
        self.log.info('Cairo UI Setup')

        self.__radius = radius
        self.__offset_lat = 0
        self.__offset_lon = 0
        self.__is_fullscreen = False
        self.__font_size = 10
        self.__dragging = False

        self.collector = LostAircraftCollector(tracking_context)

        self.window = Gtk.Window()
        self.window.add_events(Gdk.EventMask.SCROLL_MASK)
        self.window.connect('destroy', Gtk.main_quit)
        self.window.connect('key-release-event', self.on_key_release_event)
        self.window.connect('realize', self.on_window_realize)

        self.window.connect('scroll-event', self.on_scroll_event)
        self.window.connect('button-press-event', self.on_button_press)
        self.window.connect('button-release-event', self.on_button_release)
        self.window.connect('motion-notify-event', self.on_motion)

        self.window.connect('window-state-event', self.on_window_state_event)
        self.window.set_default_size(800, 600)

        self.drawingarea = Gtk.DrawingArea()
        self.window.add(self.drawingarea)
        self.drawingarea.connect('draw', self.draw)
        self.window.show_all()

    def on_scroll_event(self, widget: Gtk.Window, ev: Gdk.EventScroll):
        if ev.direction == Gdk.ScrollDirection.UP:
            self.zoom(1 * 1.2)
            GLib.idle_add(self.drawingarea.queue_draw)
        elif ev.direction == Gdk.ScrollDirection.DOWN:
            self.zoom(1 / 1.2)
            GLib.idle_add(self.drawingarea.queue_draw)

    def on_button_press(self, widget: Gtk.Window, ev: Gdk.EventButton):
        self.__dragging = True
        self.__base_x = ev.x
        self.__base_y = ev.y
        self.__original_offset_lon = self.__offset_lon
        self.__original_offset_lat = self.__offset_lat

    def on_button_release(self, widget: Gtk.Window, ev: Gdk.EventButton):
        self.__dragging = False

    def on_motion(self, widget: Gtk.Window, ev: Gdk.EventMotion):
        if self.__dragging:
            dx = ev.x - self.__base_x
            dy = ev.y - self.__base_y

            width = self.window.get_allocated_width()
            dx *= 3 * self.__radius / width
            dy *= 3 * self.__radius / width

            self.__offset_lat = self.__original_offset_lat + dy
            self.__offset_lon = self.__original_offset_lon - dx
            GLib.idle_add(self.drawingarea.queue_draw)

    def run(self):
        self.collector.start()
        Gtk.main()
        self.log.info('Cairo UI Ended')

    def aircraft_updated(self, aircraft: Aircraft):
        self.log.debug('Aircraft update:' + str(aircraft))
        GLib.idle_add(self.drawingarea.queue_draw)

    def zoom(self, factor):
        self.__radius /= factor
        GLib.idle_add(self.drawingarea.queue_draw)

    def on_window_realize(self, widget: Gtk.Window):
        # Hide cursor in window
        # c = Gdk.Cursor.new_from_name(widget.get_display(), 'none')
        # self.window.get_window().set_cursor(c)
        pass

    def on_window_state_event(self, widget: Gtk.Window, ev):
        self.__is_fullscreen = bool(ev.new_window_state & Gdk.WindowState.FULLSCREEN)

    def on_key_release_event(self, widget: Gtk.Window, ev, data=None):
        if ev.keyval == Gdk.KEY_Escape or ev.keyval == Gdk.KEY_q:
            Gtk.main_quit()
        elif ev.keyval == 45:
            self.zoom(1 / 1.2)
        elif ev.keyval == 61:
            self.zoom(1.2)
        elif ev.keyval == Gdk.KEY_F11 or ev.keyval == Gdk.KEY_f:
            if self.__is_fullscreen:
                self.window.unfullscreen()
            else:
                self.window.fullscreen()
        elif ev.keyval == 65362:
            # Up
            self.__offset_lat += 0.1
            GLib.idle_add(self.drawingarea.queue_draw)
        elif ev.keyval == 65364:
            # Down
            self.__offset_lat -= 0.1
            GLib.idle_add(self.drawingarea.queue_draw)
        elif ev.keyval == 65361:
            # Left
            self.__offset_lon -= 0.1
            GLib.idle_add(self.drawingarea.queue_draw)
        elif ev.keyval == 65363:
            # Right
            self.__offset_lon += 0.1
            GLib.idle_add(self.drawingarea.queue_draw)
        elif ev.keyval == Gdk.KEY_z:
            self.__offset_lon = 0
            self.__offset_lat = 0
            self.__radius = radius
            GLib.idle_add(self.drawingarea.queue_draw)

    def draw(self, da: Gtk.DrawingArea, ctx: Context):
        ts = time.time()
        self.log.debug('Cairo UI Drawing...')
        rect = da.get_allocation()
        projection = Projection(rect.width, rect.height, center_coord[0] + self.__offset_lat,
                                center_coord[1] + self.__offset_lon, self.__radius)

        ctx.set_source_rgb(0, 0, 0)
        ctx.rectangle(0, 0, rect.width, rect.height)
        ctx.fill()

        ctx.set_font_size(self.__font_size)
        ctx.select_font_face('monospace', cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

        self.draw_grid(ctx, projection, rect)
        self.draw_circles(ctx, projection)
        self.draw_cities(ctx, projection)
        self.draw_waypoints(ctx, projection)
        self.draw_airports(ctx, projection)
        self.draw_aircrafts(ctx, projection)
        self.draw_information(ctx)
        self.log.debug('Cairo UI Drawing done in %4dms' % (1000 * (time.time() - ts)))

    def draw_cities(self, ctx: Context, projection: Projection):
        ctx.save()

        for city in cities:
            city_name = city[0]
            city_screen = projection.get_screen_coordinates(city[1][0], city[1][1])

            ctx.set_source_rgb(0.1, 0.35, 0.15)
            ctx.arc(city_screen[0], city_screen[1], 4, 0, 2 * math.pi)
            ctx.fill()

            ctx.set_source_rgb(0.2, 0.7, 0.3)
            ctx.move_to(city_screen[0], city_screen[1])
            ctx.show_text(city_name)

        ctx.restore()

    def draw_waypoints(self, ctx: Context, projection: Projection):
        ctx.save()
        ctx.set_source_rgba(0.20, 0.80, 0.90, 0.20)
        ctx.set_dash([5, 4, 2, 4])

        for route in routes:
            wp1 = waypoints[route[0]]
            wp2 = waypoints[route[1]]

            sc1 = projection.get_screen_coordinates_60(wp1[0], wp1[1])
            sc2 = projection.get_screen_coordinates_60(wp2[0], wp2[1])

            ctx.new_path()
            ctx.move_to(sc1[0], sc1[1])
            ctx.line_to(sc2[0], sc2[1])
            ctx.stroke()

        ctx.set_source_rgba(0.20, 0.80, 0.90, 0.50)
        ctx.set_dash([])

        for wp_name, wp_data in waypoints.items():
            wp_screen = projection.get_screen_coordinates_60(wp_data[0], wp_data[1])
            wp_edges = wp_data[2]

            ctx.new_path()
            rad = 6
            a = 0

            while a <= 360:
                x = wp_screen[0] + rad * math.cos(a * math.pi / 180)
                y = wp_screen[1] + rad * math.sin(a * math.pi / 180)

                if a == 0:
                    ctx.move_to(x, y)
                else:
                    ctx.line_to(x, y)

                a += 360 / wp_edges

            ctx.stroke()

            ctx.move_to(wp_screen[0] + 8, wp_screen[1])
            ctx.show_text(wp_name)

        ctx.restore()

    def draw_circles(self, ctx: Context, projection: Projection):
        ctx.save()
        center = projection.get_screen_coordinates(center_coord[0], center_coord[1])
        ctx.set_line_width(2)

        for r in range(4, 15, 3):
            if r == 10:
                ctx.set_dash([])
                ctx.set_source_rgba(0, 1, 0, 0.30)
            else:
                ctx.set_dash([5, 15])
                ctx.set_source_rgba(0, 1, 0, 0.20)

            ctx.arc(center[0], center[1], projection.get_pixels(r / 10), 0, 2 * math.pi)
            ctx.stroke()

        ctx.restore()

    def draw_grid(self, ctx: Context, projection: Projection, rect):
        ctx.save()
        ctx.set_source_rgba(0.3, 0.6, 1, 0.2)
        # ctx.set_dash([1, 3])

        for l, x in projection.get_grid_steps_x(1):
            ctx.move_to(x, 0)
            ctx.line_to(x, rect.height)
            ctx.stroke()

            ctx.move_to(x, rect.height * 0.5)
            ctx.rotate(-45)
            ctx.show_text(str(l))
            ctx.rotate(45)

        for l, y in projection.get_grid_steps_y(1):
            ctx.move_to(rect.width * 0.5, y)
            ctx.show_text(str(l))

            ctx.move_to(0, y)
            ctx.line_to(rect.width, y)
            ctx.stroke()

        ctx.restore()

    def draw_aircrafts(self, ctx: Context, projection: Projection):

        ctx.save()
        ctx.set_font_size(12)
        interline = 14
        r = 0.8
        g = 0.7
        b = 0.3

        for aircraft in self.tracking_context.aircrafts:
            if aircraft.lat is not None and aircraft.lon is not None:
                aircraft_screen = projection.get_screen_coordinates(aircraft.lat, aircraft.lon)
                base_level = 0.85 if aircraft.getTrackingStatus() else 0.4

                if len(aircraft.trajectory.positions) > 1:
                    ctx.save()
                    ctx.set_line_width(3)
                    ctx.set_source_rgba(r, g, b, base_level * 0.3)
                    ps = projection.get_screen_coordinates(
                        aircraft.trajectory.positions[0].lat,
                        aircraft.trajectory.positions[0].lon
                    )
                    ctx.move_to(ps[0], ps[1])

                    for pos in aircraft.trajectory.positions[1:]:
                        ps = projection.get_screen_coordinates(pos.lat, pos.lon)
                        ctx.line_to(ps[0], ps[1])

                    ctx.stroke()
                    ctx.restore()

                if aircraft.heading is not None:
                    ctx.save()
                    ctx.set_source_rgba(r, g, b, base_level * 0.9)
                    ctx.set_dash([2, 1])
                    ctx.move_to(aircraft_screen[0], aircraft_screen[1])
                    ctx.rotate(-math.pi / 2 + aircraft.heading * math.pi / 180)
                    ctx.rel_line_to(40, 0)
                    ctx.stroke()
                    ctx.restore()

                ctx.set_source_rgba(r, g, b, base_level * 0.8)
                ctx.arc(aircraft_screen[0], aircraft_screen[1], 6, 0, 2 * math.pi)
                ctx.fill()

                ctx.set_source_rgba(r, g, b, base_level)
                ctx.move_to(aircraft_screen[0] + 6, aircraft_screen[1])
                ctx.show_text(aircraft.callsign)

                ctx.set_source_rgba(r, g, b, base_level * 0.9)
                ctx.move_to(aircraft_screen[0] + 6, aircraft_screen[1] + 1 * interline)
                ctx.show_text(' Hdg: ' + str(aircraft.heading))
                ctx.move_to(aircraft_screen[0] + 6, aircraft_screen[1] + 2 * interline)
                ctx.show_text(' Alt: ' + str(aircraft.altitude) + ' ft')
                ctx.move_to(aircraft_screen[0] + 6, aircraft_screen[1] + 3 * interline)
                ctx.show_text('HSpd: ' + str(aircraft.speed) + ' kt')
                ctx.move_to(aircraft_screen[0] + 6, aircraft_screen[1] + 4 * interline)
                ctx.show_text('VSpd: ' + str(aircraft.vspeed) + ' ft/min')

        ctx.restore()

    def draw_airports(self, ctx: Context, projection: Projection):

        ctx.save()

        ctx.set_source_rgba(0.90, 0.92, 0.96, 0.7)
        ctx.set_line_width(5)

        for ap_name, ap_coord1, ap_coord2 in airport_coords:
            sc_coord1 = projection.get_screen_coordinates(ap_coord1[0], ap_coord1[1])
            sc_coord2 = projection.get_screen_coordinates(ap_coord2[0], ap_coord2[1])
            ctx.move_to(sc_coord1[0], sc_coord1[1])
            ctx.line_to(sc_coord2[0], sc_coord2[1])
            ctx.stroke()

            ctx.move_to(max(sc_coord1[0], sc_coord2[0]), max(sc_coord1[1], sc_coord2[1]))
            ctx.show_text(ap_name)

        ctx.restore()

    def draw_information(self, ctx):
        ctx.save()

        ctx.set_font_size(12)
        step = 16

        ctx.set_source_rgba(0.18, 0.44, 0.88, 0.8)
        ctx.move_to(10, 10 + 1 * step)
        ctx.show_text('    Tracked Objects: ' + '{0: >3d}'.format(len(self.tracking_context.aircrafts)))
        ctx.move_to(10, 10 + 2 * step)
        ctx.show_text('Messages per second: ' + self.adsb_monitor.get_messages_per_second_as_string(time.time()))
        ctx.move_to(10, 10 + 3 * step)
        ctx.show_text('         Error rate: ' + self.adsb_monitor.get_error_rate_as_string())

        ctx.restore()

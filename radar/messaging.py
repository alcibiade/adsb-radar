from threading import Thread

import orjson
from redis import StrictRedis

from adsb import AdsbProcessor, AdsbProducer
from model import AdsbMessage, from_json


class RedisTopicPublisher(AdsbProcessor):
    def __init__(self, host: str, channel: str):
        super(AdsbProcessor).__init__()
        self.r = StrictRedis(host=host)
        self.channel = channel

    def process_adsb(self, message: AdsbMessage):
        self.r.publish(self.channel, orjson.dumps(message))


class RedisTopicConsumer(AdsbProducer, Thread):
    def __init__(self, host: str, channel: str, processor: AdsbProcessor):
        Thread.__init__(self)
        AdsbProducer.__init__(self, processor=processor)

        self.r = StrictRedis(host=host)
        self.p = self.r.pubsub()
        self.p.subscribe([channel])

    def run(self):
        for msg in self.p.listen():
            # print(msg)
            if msg['type'] == 'message':
                message = from_json(msg['data'].decode('ascii'))
                self.next_processor.process_adsb(message)


class RedisQueuePublisher(AdsbProcessor):
    def __init__(self, host: str, listname: str):
        super(AdsbProcessor).__init__()
        self.r = StrictRedis(host=host)
        self.listname = listname

    def process_adsb(self, message: AdsbMessage):
        self.r.lpush(self.listname, orjson.dumps(message))


class RedisQueueConsumer(AdsbProducer, Thread):
    def __init__(self, host: str, listname: str, processor: AdsbProcessor):
        Thread.__init__(self)
        AdsbProducer.__init__(self, processor=processor)

        self.r = StrictRedis(host=host)
        self.listname = listname

    def run(self):
        while True:
            listname, text = self.r.brpop(self.listname)
            message = from_json(text.decode('ascii'))
            self.next_processor.process_adsb(message)

import copy
import itertools
from typing import Tuple

import pyModeS as pms

from fastcrc import crc
from model import AdsbMessage


def correct(adsb_message: AdsbMessage, max_correction: int) -> Tuple[AdsbMessage, int]:
    msg = adsb_message.message
    initial_crc = crc(msg)
    result = (None, 99)

    if initial_crc == 0:
        result = (adsb_message, 0)
    else:
        for correction in range(1, max_correction + 1):
            msg_bin = pms.hex2bin(msg)
            for patches in itertools.combinations(range(len(msg_bin)), correction):
                for i in patches:
                    if msg_bin[i] == '0':
                        msg_bin = replace(msg_bin, i, '1')
                    else:
                        msg_bin = replace(msg_bin, i, '0')

                patched_msg = hex(int(msg_bin, 2))[2:]
                if crc(patched_msg) == 0:
                    clone = copy.copy(adsb_message)
                    clone.message = patched_msg
                    return (clone, correction)

                for i in patches:
                    if msg_bin[i] == '0':
                        msg_bin = replace(msg_bin, i, '1')
                    else:
                        msg_bin = replace(msg_bin, i, '0')

    return result


def replace(msg_bin, i, s):
    return msg_bin[:i] + s + msg_bin[i + 1:]
